from bs4 import BeautifulSoup
from time import sleep
import os
import re
import wget
import requests

BASE_URL = "https://www.aparat.com"
PLAY_LIST_URL = ""
OUT_PATH_DIR = ""
OFFLINE_MODE = False
OFFLINE_HTML_FILE = ""

HEADERS = {'user-agent': 'Mozilla/6.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Chrome/66'}
# for separate saving name and link in txt file
LINE_SEP = "<@@@>"


def get_all_links_online(url):
    """
    get all a tag by request playlist url
    :param url: play list link such as https://www.aparat.com/playlist/288572
    :return: request result form url
    """
    request_links = requests.get(url, headers=HEADERS)
    result_request = BeautifulSoup(request_links.content, "html.parser")
    playlist_links = result_request.findAll('a')
    return playlist_links


def generate_episode_links_online(lst_web_content):
    """
    generate base link for each episode in playlist  as list links form lst_web_content
    :param lst_web_content:request result
    :return: list of item contain dict name and txt links of episodes
    """
    lst_duplicate_link = []
    lst_download_links = []
    lst_file_names = []
    counter = 0
    for episode_link in lst_web_content:
        try:
            result_link = str(episode_link.get('href'))
            if result_link.startswith("/v") and len(result_link) > 100 and "playlist" in result_link:
                episode_url = BASE_URL + result_link
                download_link = generate_episode_download_link(episode_url)
                file_name = str(os.path.basename(download_link['link']))
                if file_name is None or lst_duplicate_link.count(file_name) > 0:
                    continue
                final_fix_format_file_name = file_name.split('-')
                lst_download_links.append(download_link)
                lst_duplicate_link.append(file_name)
                lst_file_names.append(final_fix_format_file_name[0] + LINE_SEP + download_link['filename'])
                counter += 1
                print(counter, " ", download_link)
        except Exception as e:
            print(e, result_link)
    lst_pure_links = [link['link'] for link in lst_download_links]
    log_content_to_txt_file(lst_pure_links, "download_links.txt")
    log_content_to_txt_file(lst_file_names, "download_file_names.txt")
    return lst_download_links


def generate_episode_download_link(url):
    """
    generate episode mp4 file link form apart site by quality
    start form 720p and end with 1080p (1080p is end because for large size)
    :param url: episode url link
    :return: list contain dict for files with name and url of file
    """
    request_res = requests.get(url, headers=HEADERS)
    final_links = BeautifulSoup(request_res.content, "html.parser")
    file_name = str(final_links.title.text).strip().replace('  ', '')
    all_links = final_links.find_all('a')
    quality_range = ('720p', '480p', '360p', '240p', '1080p')
    for file_quality in quality_range:
        for my_link in all_links:
            result_link_end = str(my_link.get('href'))
            if result_link_end.endswith('.mp4') and file_quality in result_link_end:
                return {"filename": file_name, "link": result_link_end}
    return {"filename": "", "link": ""}


def get_all_links_from_html_file(html_file):
    link_pattern = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    with open(html_file, 'r', encoding="utf-8")as f:
        html_content = f.read()
        lst_links = re.findall(link_pattern, html_content)
        episode_code = str(PLAY_LIST_URL).split('/playlist/')[1]
        lst_final_links = [link for link in lst_links if episode_code in link]
    return lst_final_links


def generate_episode_links_form_html_file(playlist_links):
    """
    generate list of episode playlist links form apart site
    :return: list txt links of episode playlist
    """
    lst_duplicate_link = []
    lst_download_links = []
    lst_file_names = []
    counter = 0
    for episode_link in playlist_links:
        try:
            if episode_link.startswith("http") and "playlist" in episode_link:
                download_link = generate_episode_download_link(episode_link)
                file_name = str(os.path.basename(download_link['link']))
                if file_name is None or lst_duplicate_link.count(file_name) > 0:
                    continue
                final_fix_format_file_name = file_name.split('-')
                lst_download_links.append(download_link)
                lst_duplicate_link.append(file_name)
                lst_file_names.append(final_fix_format_file_name[0] + LINE_SEP + download_link['filename'])
                counter += 1
                print(counter, " ", download_link)
        except Exception as e:
            print(e, episode_link)
    lst_pure_links = [link['link'] for link in lst_download_links]
    log_content_to_txt_file(lst_pure_links, "download_links.txt")
    log_content_to_txt_file(lst_file_names, "download_file_names.txt")
    return lst_download_links


def download_play_list_files(lst_download_links_dict, out_path_dir):
    """
    download file form list contain dict
    :param lst_download_links_dict:
    :param out_path_dir: save download files
    :return: no this !!!
    """
    if not os.path.exists(out_path_dir):
        os.mkdir(out_path_dir)
    for download_item in lst_download_links_dict:
        try:
            if download_item is not None:
                url_link = download_item["link"]
                file_final_path = os.path.join(out_path_dir, os.path.basename(url_link).split('-')[0] + '.mp4')
                if not os.path.exists(file_final_path):
                    print("download start : " + url_link)
                    wget.download(url_link, file_final_path)
                    sleep(2)
                else:
                    print(download_item[" filename "] + 'is exists')
        except Exception as e:
            print(e, download_item["filename"])


def rename_file_to_new_format(dir_path):
    lst_all_files = os.listdir(dir_path)
    for file in lst_all_files:
        try:
            if not file.endswith('.mp4') and "-720" not in file:
                continue
            new_file_name = file.split('-720')[0]
            input_file = os.path.join(dir_path, file)
            output_file = os.path.join(dir_path, new_file_name) + '.mp4'
            os.rename(input_file, output_file)
        except Exception as e:
            print(e)


def remove_extra_char_in_persian_name(farsi_name):
    """
    remove extra char for farsi name
    :param farsi_name:
    :return: clean str of farsi file name
    """
    if farsi_name is not None:
        clean_extra_char_farsi_name = re.sub(r"[^\w.  ]+", '', farsi_name)
        final_farsi_name = str(clean_extra_char_farsi_name).strip() + '.mp4'
        return final_farsi_name
    else:
        print('invalid name !!!')


def rename_download_files_to_persian_name(txt_file, dir_path):
    file = open(txt_file, 'r', encoding='utf-8')
    lst_file_names = file.readlines()
    for item in lst_file_names:
        english_name, farsi_name = item.split(LINE_SEP)
        final_farsi_name = remove_extra_char_in_persian_name(farsi_name)
        current_file_name = os.path.join(dir_path, english_name + '.mp4')
        if os.path.exists(current_file_name):
            os.rename(current_file_name, os.path.join(dir_path, final_farsi_name))
        else:
            print(english_name + "  not find  !!!!!!!!!! ")


def log_content_to_txt_file(lst_links_text, my_file):
    """
    log all content to txt file
    :param lst_links_text: list of item to save in txt file
    :param my_file:file name
    :return: none
    """
    ls = list(lst_links_text)
    with open(my_file, 'w', encoding="utf-8")as f:
        for item in ls:
            if item is not None:
                f.write(item + "\n")


def start():
    print("start generate download links")
    if OFFLINE_MODE:
        lst_links = get_all_links_from_html_file(OFFLINE_HTML_FILE)
        generate_episode_links_form_html_file(lst_links)
    else:
        web_req_result = get_all_links_online(PLAY_LIST_URL)
        lst_links = generate_episode_links_online(web_req_result)
    if lst_links:
        print("start download files !!! \n")
        download_play_list_files(lst_links, OUT_PATH_DIR)
        rename_download_files_to_persian_name("download_file_names.txt", OUT_PATH_DIR)
        print("end downloads !!!")


if __name__ == '__main__':
    OFFLINE_MODE = False
    OUT_PATH_DIR = "playlist_name"
    OFFLINE_HTML_FILE = ""
    PLAY_LIST_URL = "https://www.aparat.com/playlist/117849"
    start()
